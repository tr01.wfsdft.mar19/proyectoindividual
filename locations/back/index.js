const config        = require('./config');
const express       = require('express');
const path          = require('path');
const http          = require('http');
const port          = process.env.PORT || config.http.port;
const jwt           = require('jsonwebtoken');
const cors          = require('cors');

const indexRouter   = require('./routes/index');
const authRouter    = require('./routes/auth');

const index = express();

index.use(cors({
  origin: 'http://localhost:4200'
}));
index.use(express.json());
index.use(express.urlencoded({ extended: false }));
index.use(express.static(path.join(__dirname, 'public')));

const authMiddleware = async(request, response, next)=>{
  //console.log(request.query.authorization);
  if(request.headers.authorization){
    // let bearer = request.headers.authorization;
    // let token = bearer.split(" ")[1];
    let token = request.headers.authorization;
    jwt.verify(token, config.jwt.api_key, { algorithm: "HS512" , expiresIn: 120 }) ?
      await next() : response.json({'ok': false, fails:[{"error": "Unauthorized access."}]});
  }
};

index.use('/', authRouter);
index.use('/', indexRouter);
index.set('port', port);

http.createServer(index).listen(port);


