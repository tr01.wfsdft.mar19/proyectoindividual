const express       = require('express');
const router        = express.Router();
const jwt           = require('jsonwebtoken');
const config        = require('../config');

router.get('/auth', async(request, response, next)=>{

  response.header('Content-Type', 'application/json');
  response.header('Access-Control-Allow-Origin', '*');
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  response.header('Access-Control-Allow-Credentials', 'true');

  const generateUUID = ()=>{
    let date = new Date().getTime();
    let uuid = 'xxxxxxxx-yxxx-4xxxxxxx-xxxxx-xxxxxxxx'.replace(/[xy]/g, function(u){
      let reg = (date + Math.random()*32)%32 | 0;
      date = Math.floor(date/32);
      return (u === 'x' ? reg: (reg&0x3|0x8)).toString(32);
    });
    return uuid;
  };

  let uuid = generateUUID();

  const header = {
    "alg": "HS512",
    "typ": "JWT"
  };

  const payload = { uuid };

  const options = {
    algorithm: "HS512",
    expiresIn: "7d"
  };

  let token = await jwt.sign(payload, config.jwt.api_key, { algorithm: "HS512" ,expiresIn: (1000*60*60*24)*7});
  if(token){
    response.json({"ok": true, data:token, fails:[]});
  }else{
    response.json({"ok": false, data:[], fails:[{error: "error getting token."}]});
  }
});

module.exports = router;
