const express         = require('express');
const router          = express.Router();
const config        = require('../config.json');
const MongoClient   = require('mongodb').MongoClient;
const ObjectId      = require('mongodb').ObjectId;

const getMongoClient = async()=>{
  return new Promise((resolve, reject)=>{
    const url = config.db.url;

    MongoClient.connect(url, { useNewUrlParser: true }, async(error, client)=>{
      //let mClient;
      if(error){
        console.log('MongoDB: connection error.');
        reject(error);
      }else{
        resolve(client);
      }
    });
  });
};

/* GET home page.
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
 */

router.get('/locations', async(request, response, next)=>{
  const client = await getMongoClient();
  await client.db('locations').collection('locations').find({}).toArray(async(error, locations)=>{
    if(error){
      return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }else{
      return response.json({ ok: true, data: locations, fails:[] });
    }
  });
});

router.get('/locations/:id', async(request, response, next)=>{
  const client = await getMongoClient();
  const id = request.params.id;

  await client.db('locations').collection('locations').find({"_id": ObjectId(id)}).toArray(async(error, location)=>{
    if(error){
      return response.status(500).json({ ok: false, data: [], fails:[{error: error}] });
    }
    return response.status(200).json({ ok: true, data: location, fails: [] });
  });
});

router.post('/locations', async(request, response, next)=>{
  const client    = await getMongoClient();
  const country   = request.body.Country;
  const city      = request.body.City;
  const lat       = request.body.location.coordinates[0];
  const lng       = request.body.location.coordinates[1];

  let location = {
    "Country": country,
    "City": city,
    "location":{
      "Type": "Point",
      "coordinates": [
        lat,
        lng
      ]
    }
  };

  if(location){
    await client.db(config.db.db).collection(config.db.collection).insertOne(location, (error, result)=>{
      if(error){
        return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
      }
      return response.status(200).json({ ok: true, data: result, fails:[] });
    });
  }
});

router.put('/locations/:id', async(request, response, next)=>{
  const client = await getMongoClient();
  const id = request.params.id;
  const country   = request.body.Country;
  const city      = request.body.City;
  const lat       = request.body.location.coordinates[0];
  const lng       = request.body.location.coordinates[1];

  /*let location = {
    "Country": country,
    "City": city,
    "location":{
      "Type": "Point",
      "coordinates": [
        lat,
        lng
      ]
    }
  };*/

  await client.db(config.db.db).collection(config.db.collection).findOneAndUpdate({"_id": ObjectId(id)}, {
    $set: {
      Country: country,
      City: city,
      location: {
        Type: "Point",
        coordinates: [
          lat,
          lng
        ]
      }
    }
  }, (error, result)=>{
    if(error){
      return response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    return response.status(200).json({ ok: true, data: result, fails:[] });
  });
});

router.delete('/locations/:id', async(request, response, next)=>{
  const client = await getMongoClient();
  const id = request.params.id;

  await client.db(config.db.db).collection(config.db.collection).findOneAndDelete({"_id": ObjectId(id)}, (error, result)=>{
    if(error){
      response.status(500).json({ ok: false, data:[], fails:[{error: error}] });
    }
    response.status(200).json({ ok: true, data:result, fails:[] });
  });
});

module.exports = router;
