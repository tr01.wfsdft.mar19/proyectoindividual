import { TestBed } from '@angular/core/testing';

import { GetMarkersService } from './get-markers.service';

describe('GetMarkersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GetMarkersService = TestBed.get(GetMarkersService);
    expect(service).toBeTruthy();
  });
});
