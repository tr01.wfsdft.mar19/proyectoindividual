import {Component, OnInit, AfterViewInit, ViewChild, ElementRef} from '@angular/core';
import {} from 'googlemaps';
import { GetMarkersService } from "../get-markers.service";

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit, AfterViewInit {

  @ViewChild('map') map: google.maps.Map;
  token: any;
  locations: [];
  markers: [];
  mapMarkers: Array<google.maps.Marker>=[];
  country:  any;
  city:  any;
  currentPosition:  any;
  currentPosMarker:  any;
  latitude: any;
  longitude: any;
  lat: number = 40.416775;
  lng: number = -3.70379;
  zoom: number = 6;
  constructor(private getMarkersService: GetMarkersService) { }

  async ngOnInit() {
    /*await fetch("http://127.0.0.1:3000/auth", {
      mode: "cors"
    })
      .then(response => {
        return response.json();
      })
      .then(data => {
        //console.log(data.data);
        this.token = data.data;
        console.log(this.token);
      })
      .catch(error => {
        console.error(error);
      });*/

    /*this.getMarkersService.getAllMarkers().subscribe(data=>{
      this.locations = data['data'];
      this.locations.forEach(location=>{
        let latLng = new google.maps.LatLng({
          lat: location['location']['coordinates'][0],
          lng: location['location']['coordinates'][1]
        });
        let mapMarker = new google.maps.Marker({
          position: latLng,
          draggable: true,
          clickable: true
        });
        this.mapMarkers.push(new google.maps.Marker({
          position: latLng,
          draggable: true,
          clickable: true
        }));
      });
    });*/
  }

  async ngAfterViewInit() {
    await this.getAllMarkers();
  }

  async getAllMarkers() {
    this.getMarkersService.getAllMarkers().subscribe(data=>{
      this.locations = data['data'];
      console.log(this.locations);
      this.locations.forEach(location=>{
        let latLng = new google.maps.LatLng({
          lat: location['location']['coordinates'][0],
          lng: location['location']['coordinates'][1]
        });
        let mapMarker = new google.maps.Marker({
          position: latLng,
          draggable: true,
          clickable: true
        });
        this.mapMarkers.push(new google.maps.Marker({
          position: latLng,
          draggable: true,
          clickable: true
        }));
      });
    });
  }
}

