import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GetMarkersService {

  markers: [];
  constructor(private http: HttpClient) { }

  getAllMarkers() {
    return this.http.get<object[]>(`http://127.0.0.1:3000/locations`);
  }
}
